import React, {useState, useEffect} from 'react';
import io from 'socket.io-client'
import queryString from 'query-string';
import InfoBar from '../InfoBar/InfoBar'
import Input from '../Input/Input'
import Messages from '../Messages/Messages'
import SideBarInfo from '../SideBarInfo/SideBarInfo'
import './Chat.css'

const ENDPOINT = process.env.BACKEND_URL;
let socket;

const Chat = ({location}) => {

    const [name,setName] = useState('');
    const [room,setRoom] = useState('');
    const [messages,setMessages] = useState([]);
    const [message,setMessage] = useState('');
    const [users,setUsers] = useState([]);

    useEffect( () => {
        const {name, room} = queryString.parse(location.search);
        setName(name);
        setRoom(room);
        socket = io(ENDPOINT);
        console.log(socket)

        //El tercer parametro es un callback que manda el backend con datos 
        // Para ser usado en el Front O como manejo de errores !!!!
        socket.emit('join', { name, room },() => {
            
        })
        
        return () => {
            socket.emit('disconnect')
            socket.off();
        }
    },[ENDPOINT,location.search])

    useEffect(() => {
        // evento Message es el que manda datos de el server al cliente
        socket.on('message',(message) => {
            setMessages([...messages,message]);
        })

        socket.on('roomData',(roomData) => {
            console.log(roomData)
            setUsers([...roomData.users])
        })

    },[messages])

    const sendMessage = (e) => {
        e.preventDefault();
        if(message){
            //Evento sendMessage es el que envio datos de cliente a servidor
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    }
    console.log(messages,message)
    return ( 
        
        <div className="outerContainer">
            
            <div className="container">
                
                <InfoBar room={room} />
                <Messages messages={messages} name={name} />
                <Input
                    message={message}
                    sendMessage={sendMessage}
                    setMessage={setMessage}
                />
            </div>
            <SideBarInfo users={users} />
        </div>
     );
}
 
export default Chat;