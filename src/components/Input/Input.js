import React from 'react';

import './Input.css';

const Input = ({message,sendMessage,setMessage}) => {
    return ( 
        <form action="" className="form">
            <input 
                className="input"
                type="text"
                value={message} 
                placeholder="Escribe un mensaje ..."
                onChange={e => setMessage(e.target.value)}
                onKeyPress={e => e.key === 'Enter' ? sendMessage(e) : null}
            />
            <button className="sendButton"
                onClick={e => sendMessage(e)}
            >Enviar</button>
        </form>
     );
}
 
export default Input;